package com.example.firstprojectdemo.presentation.mapper

import com.example.firstprojectdemo.domain.entities.Users
import com.example.firstprojectdemo.presentation.entities.DisplayUsers

class DisplayUserMapper {

    fun fromDomain(users: Users) = DisplayUsers(users.id, users.author, users.download_url)

}