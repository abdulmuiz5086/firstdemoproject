package com.example.firstprojectdemo.presentation.activities

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.firstprojectdemo.R

class OnStart : AppCompatActivity() {

    private lateinit var sp: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_on_start)

        sp = getSharedPreferences("login",MODE_PRIVATE)

        if(sp.getBoolean("logged", false)){
            val intent = Intent(this, Dashboard::class.java)
            startActivity(intent)
        } else {
            nextactivityscreen()
        }
    }

    private fun nextactivityscreen() {
        Handler().postDelayed({
            val intent = Intent(this, InfoPage::class.java)
            startActivity(intent)
            finish()
        }, 3000)
    }
}