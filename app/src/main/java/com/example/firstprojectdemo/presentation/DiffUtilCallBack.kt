package com.example.firstprojectdemo.presentation

import androidx.recyclerview.widget.DiffUtil
import com.example.firstprojectdemo.presentation.entities.DisplayUsers

class DiffUtilCallBack : DiffUtil.ItemCallback<DisplayUsers>() {
    override fun areItemsTheSame(oldItem: DisplayUsers, newItem: DisplayUsers): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: DisplayUsers, newItem: DisplayUsers): Boolean {
        return oldItem.id == newItem.id
                && oldItem.author == newItem.author
                && oldItem.download_url == newItem.download_url
    }
}