package com.example.firstprojectdemo.presentation.entities

data class DisplayUsers(

    val id: String,
    val author: String,
    val download_url: String
    
)
