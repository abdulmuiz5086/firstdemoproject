package com.example.firstprojectdemo.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.firstprojectdemo.R
import com.example.firstprojectdemo.presentation.entities.DisplayUsers

class CustomAdapter : PagingDataAdapter<DisplayUsers, CustomAdapter.ViewHolder>(DiffUtilCallBack()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_view_design, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        getItem(position)?.let { redditPost ->
            holder.bindPost(redditPost)
        }

    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        private var image : ImageView = itemView.findViewById(R.id.imageview)
        private val textView1: TextView = itemView.findViewById(R.id.chatname)

        fun bindPost(userEntityPost: DisplayUsers) {
            with(userEntityPost) {
                textView1.text = author
                Glide.with(itemView.context).load(userEntityPost.download_url).into(image)
            }
        }

    }
}