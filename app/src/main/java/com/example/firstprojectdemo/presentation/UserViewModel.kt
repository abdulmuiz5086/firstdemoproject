package com.example.firstprojectdemo.presentation

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.paging.ExperimentalPagingApi
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.map
import com.example.firstprojectdemo.data.UserRepoImpl
import com.example.firstprojectdemo.domain.usecases.GetUserUseCase
import com.example.firstprojectdemo.presentation.entities.DisplayUsers
import com.example.firstprojectdemo.presentation.mapper.DisplayUserMapper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class UserViewModel: ViewModel() {

    private val userRepoImpl = UserRepoImpl()
    private val getUserUseCase = GetUserUseCase(userRepoImpl)

    private val displayUserMapper = DisplayUserMapper()

    @ExperimentalPagingApi
    suspend fun fetchPosts(context: Context): Flow<PagingData<DisplayUsers>> {
        return getUserUseCase.invoke(context).cachedIn(viewModelScope).map { list ->
            list.map { users ->
                displayUserMapper.fromDomain(users)
            }
        }
    }

    class BooksViewModelFactory :
        ViewModelProvider.NewInstanceFactory() {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return UserViewModel() as T
        }
    }
}