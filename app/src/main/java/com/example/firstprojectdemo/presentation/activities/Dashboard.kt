package com.example.firstprojectdemo.presentation.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.paging.ExperimentalPagingApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.firstprojectdemo.*
import com.example.firstprojectdemo.data.db.AppDatabase
import com.example.firstprojectdemo.presentation.CustomAdapter
import com.example.firstprojectdemo.presentation.UserLoadingAdapter
import com.example.firstprojectdemo.presentation.UserViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class Dashboard : AppCompatActivity() {

    private lateinit var sp: SharedPreferences

    private lateinit var recyclerView: RecyclerView
    private var customAdapter = CustomAdapter()

    @ExperimentalPagingApi
    @SuppressLint("NotifyDataSetChanged")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        AppDatabase.getInstance(applicationContext)

        sp = getSharedPreferences("login",MODE_PRIVATE)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )

        toggle.setHomeAsUpIndicator(R.drawable.ic_action_name)
        toggle.isDrawerSlideAnimationEnabled = false
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        recyclerView = findViewById(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)


        lifecycleScope.launch {
            userViewModel.fetchPosts(this@Dashboard).collectLatest { pagingData ->
                customAdapter.submitData(pagingData)
            }
        }

        recyclerView.adapter = customAdapter
//        recyclerView.adapter = customAdapter.withLoadStateHeaderAndFooter(
//            header = UserLoadingAdapter { customAdapter.retry() },
//            footer = UserLoadingAdapter { customAdapter.retry() }
//        )
        recyclerView.adapter?.notifyDataSetChanged()


    }

    private val userViewModel: UserViewModel by lazy {
        UserViewModel.BooksViewModelFactory()
        ViewModelProvider(this)[UserViewModel::class.java]
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    fun logout(item: android.view.MenuItem) {
        sp.edit().putBoolean("logged",false).apply()
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    override fun onBackPressed() {
        val a = Intent(Intent.ACTION_MAIN)
        a.addCategory(Intent.CATEGORY_HOME)
        a.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(a)
    }

}