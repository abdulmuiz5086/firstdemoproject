package com.example.firstprojectdemo.presentation.activities

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import android.text.Editable

import android.text.TextWatcher
import com.example.firstprojectdemo.R

class MainActivity : AppCompatActivity() {

    lateinit var sp: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sp = getSharedPreferences("login",MODE_PRIVATE)

        val til1 = findViewById<TextInputLayout>(R.id.til1)
        val editTextEmail = findViewById<TextInputEditText>(R.id.text1)

        editTextEmail.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable) {
                    til1.error = null
            }
        })

        val til2 = findViewById<TextInputLayout>(R.id.til2)
        val editTextPassword = findViewById<TextInputEditText>(R.id.text2)

        editTextPassword.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable) {
                til2.error = null
            }
        })
    }

    fun openForgPass(view: View) {
        val testemail = "abdulmuiz5086@gmail.com"
        val til1 = findViewById<TextInputLayout>(R.id.til1)
        val editTextEmail = findViewById<TextInputEditText>(R.id.text1)
        val email: String = editTextEmail.text.toString().trim { it <= ' ' }
        if (email.isEmpty()) {
            til1.error = "Invalid email address"
            return
        }
        if (email != testemail) {
            til1.error = "Invalid email address"
            return
        }
        val intent = Intent(this, ForgotPassActivtiy::class.java)
        startActivity(intent)
    }

    fun userLogin(view: View) {
        val testemail = "abdulmuiz5086@gmail.com"
        val testpass= "abc@1234"
        val til1 = findViewById<TextInputLayout>(R.id.til1)
        val til2 = findViewById<TextInputLayout>(R.id.til2)
        val editTextEmail = findViewById<TextInputEditText>(R.id.text1)
        val email: String = editTextEmail.text.toString().trim { it <= ' ' }
        val editTextPassword = findViewById<TextInputEditText>(R.id.text2)
        val password: String = editTextPassword.text.toString().trim { it <= ' ' }
        if (email.isEmpty()) {
            til1.error = "Invalid email address"
            return
        }
        if (password.isEmpty()) {
            til2.error = "Required"
            return
        }
        if (email != testemail) {
            til1.error = "Invalid email address"
            return
        }
        if (password != testpass) {
            til2.error = "Invalid password"
            return
        }
        val intent = Intent(this, Dashboard::class.java)
        startActivity(intent)
        sp.edit().putBoolean("logged",true).apply()
    }

    override fun onBackPressed() {
        val a = Intent(Intent.ACTION_MAIN)
        a.addCategory(Intent.CATEGORY_HOME)
        a.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(a)
    }

}
