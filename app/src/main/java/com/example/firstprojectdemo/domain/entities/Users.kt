package com.example.firstprojectdemo.domain.entities

data class Users(

    val id: String,
    val author: String,
    val download_url: String

)
