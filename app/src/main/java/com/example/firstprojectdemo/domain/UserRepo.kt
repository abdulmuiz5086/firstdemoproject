package com.example.firstprojectdemo.domain

import android.content.Context
import androidx.paging.PagingData
import com.example.firstprojectdemo.domain.entities.Users
import kotlinx.coroutines.flow.Flow

interface UserRepo {

    suspend fun fetchPosts(context: Context): Flow<PagingData<Users>>

}