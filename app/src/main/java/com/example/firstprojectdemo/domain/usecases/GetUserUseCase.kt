package com.example.firstprojectdemo.domain.usecases

import android.content.Context
import androidx.paging.ExperimentalPagingApi
import com.example.firstprojectdemo.data.UserRepoImpl

class GetUserUseCase(private val userRepoImpl: UserRepoImpl) {

    @ExperimentalPagingApi
    suspend operator fun invoke(context: Context) = userRepoImpl.fetchPosts(context)
}