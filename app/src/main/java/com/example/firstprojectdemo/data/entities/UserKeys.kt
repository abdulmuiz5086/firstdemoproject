package com.example.firstprojectdemo.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class UserKeys(
    @PrimaryKey
    val id: String,
    val prevKey: Int?,
    val nextKey: Int?
)
