package com.example.firstprojectdemo.data

import android.content.Context
import androidx.paging.*
import com.example.firstprojectdemo.data.db.AppDatabase
import com.example.firstprojectdemo.data.mapper.UserDataMapper
import com.example.firstprojectdemo.data.network.ApiInterface
import com.example.firstprojectdemo.domain.UserRepo
import com.example.firstprojectdemo.domain.entities.Users
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class UserRepoImpl : UserRepo {

    companion object {
        const val DEFAULT_PAGE_INDEX = 1
    }

    @ExperimentalPagingApi
    override suspend fun fetchPosts(context: Context): Flow<PagingData<Users>> {

        val userService = ApiInterface.ApiClient.getClient
        val database = AppDatabase.getInstance(context)

        val userDataMapper = UserDataMapper()

        val pager = Pager(
            PagingConfig(pageSize = 40, enablePlaceholders = false, prefetchDistance = 3),
            remoteMediator = UserMediator(userService, database),
            pagingSourceFactory = { database.userDao().loadUsers() }
        ).flow

        val user = pager.map { list ->
            list.map { element ->
                userDataMapper.fromDb(element)
            }
        }

        return user

    }

}
