package com.example.firstprojectdemo.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class UserEntity(

    @SerializedName("id")
    @PrimaryKey
    var id: String,
    @SerializedName("author")
    val author: String,
    @SerializedName("download_url")
    val download_url: String
)
