package com.example.firstprojectdemo.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.firstprojectdemo.data.entities.UserEntity
import com.example.firstprojectdemo.data.entities.UserKeys

@Database(entities = [UserEntity::class, UserKeys::class], version = 4)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun userKeysDao(): UserKeysDao

    companion object {
        private var instance: AppDatabase? = null

        @Synchronized
        fun getInstance(ctx: Context): AppDatabase {
            if(instance == null)
                instance = Room.databaseBuilder(ctx, AppDatabase::class.java, "note_database")
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build()

            return instance!!
        }
    }
}