package com.example.firstprojectdemo.data

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.example.firstprojectdemo.data.UserRepoImpl.Companion.DEFAULT_PAGE_INDEX
import com.example.firstprojectdemo.data.db.AppDatabase
import com.example.firstprojectdemo.data.entities.UserEntity
import com.example.firstprojectdemo.data.entities.UserKeys
import com.example.firstprojectdemo.data.network.ApiInterface
import retrofit2.HttpException
import java.io.IOException
import java.io.InvalidObjectException

@ExperimentalPagingApi
class UserMediator(
    private val userService: ApiInterface,
    private val userDatabase: AppDatabase
) : RemoteMediator<Int, UserEntity>() {

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, UserEntity>
    ): MediatorResult {

        val page = when (val pageKeyData = getKeyPageData(loadType, state)) {
            is MediatorResult.Success -> {
                return pageKeyData
            }
            else -> {
                pageKeyData as Int
            }
        }

        return try {

            val response = userService.getMovies(page, state.config.pageSize)
            val isEndOfList = response.isEmpty()

            userDatabase.withTransaction {

                val prevKey = if (page == DEFAULT_PAGE_INDEX) null else page - 1
                val nextKey = if (isEndOfList) null else page + 1

                val keys = response.map {
                    UserKeys(id = it.id, prevKey = prevKey, nextKey = nextKey)
                }

                userDatabase.userKeysDao().saveUserKeys(keys)
                userDatabase.userDao().insertAll(response)
            }
            return MediatorResult.Success(endOfPaginationReached = isEndOfList)
        } catch (exception: IOException) {
            MediatorResult.Error(exception)
        } catch (exception: HttpException) {
            MediatorResult.Error(exception)
        }

    }

    private suspend fun getKeyPageData(loadType: LoadType, state: PagingState<Int, UserEntity>): Any? {
        return when (loadType) {
            LoadType.REFRESH -> {
                val userKeys = getClosestRemoteKey(state)
                userKeys?.nextKey?.minus(1) ?: DEFAULT_PAGE_INDEX
            }
            LoadType.APPEND -> {
                val userKeys = getLastRemoteKey(state)
                    ?: throw InvalidObjectException("Remote key should not be null for $loadType")
                userKeys.nextKey
            }
            LoadType.PREPEND -> {
                val userKeys = getFirstRemoteKey(state)
                    ?: throw InvalidObjectException("Invalid state, key should not be null")
                userKeys.prevKey ?: return MediatorResult.Success(endOfPaginationReached = true)
                userKeys.prevKey
            }
        }
    }

    private suspend fun getLastRemoteKey(state: PagingState<Int, UserEntity>): UserKeys? {
        return state.pages
            .lastOrNull { it.data.isNotEmpty() }
            ?.data?.lastOrNull()
            ?.let { user -> userDatabase.userKeysDao().getKeysId(user.id) }
    }

    private suspend fun getFirstRemoteKey(state: PagingState<Int, UserEntity>): UserKeys? {
        return state.pages
            .firstOrNull { it.data.isNotEmpty() }
            ?.data?.firstOrNull()
            ?.let { user -> userDatabase.userKeysDao().getKeysId(user.id) }
    }

    private suspend fun getClosestRemoteKey(state: PagingState<Int, UserEntity>): UserKeys? {
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.id?.let { id ->
                userDatabase.userKeysDao().getKeysId(id)
            }
        }
    }
}