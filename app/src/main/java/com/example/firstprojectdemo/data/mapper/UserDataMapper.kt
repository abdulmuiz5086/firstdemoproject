package com.example.firstprojectdemo.data.mapper

import com.example.firstprojectdemo.data.entities.UserEntity
import com.example.firstprojectdemo.domain.entities.Users

class UserDataMapper {

    fun fromDb(userEntity: UserEntity) = Users(userEntity.id, userEntity.author, userEntity.download_url)

}