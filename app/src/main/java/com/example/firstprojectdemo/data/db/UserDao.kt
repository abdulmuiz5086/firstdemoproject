package com.example.firstprojectdemo.data.db

import androidx.paging.PagingSource
import androidx.room.*
import com.example.firstprojectdemo.data.entities.UserEntity

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(userEntity: List<UserEntity>)

    @Query("SELECT * FROM userentity")
    fun loadUsers(): PagingSource<Int, UserEntity>

}