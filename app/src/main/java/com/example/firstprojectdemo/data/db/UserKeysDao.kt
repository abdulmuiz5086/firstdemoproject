package com.example.firstprojectdemo.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.example.firstprojectdemo.data.entities.UserKeys

@Dao
interface UserKeysDao {

    @Insert(onConflict = REPLACE)
    fun saveUserKeys(userKey: List<UserKeys>)

    @Query("SELECT * FROM userkeys WHERE id = :id")
    suspend fun getKeysId(id: String): UserKeys?

}